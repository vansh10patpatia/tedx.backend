require('dotenv').config()
var express = require("express");
var path = require("path");
var mongoose = require("mongoose");
var bodyParser = require("body-parser");
const cors = require('cors');
var app = express();
app.use(bodyParser.json());
app.use(cors());
app.options('*', cors());
app.use(bodyParser.urlencoded({ extended: true }));
// let server;
mongoose.connect(process.env.MONGO_URI).then(() => {
  console.log('Connected to MongoDB');
  app.listen(process.env.PORT || 8000, () => {
    console.log(`Listening to port ${process.env.PORT}`);
  });
});

var FormSchema = new mongoose.Schema({
  name: String,
  email: String,
  body: String,
  created_at: { type: Date, default: Date.now },
});
var TransactionSchema = new mongoose.Schema({
  name: String,
  email: String,
  transactionId: {
    type:String,
    unique: true,
  },
  contact: String,
  verified:false,
  created_at: { type: Date, default: Date.now },
});
var LoginSchema = new mongoose.Schema({
  email:String,
  password:String
})


mongoose.model("Login",LoginSchema)
var Login = mongoose.model("Login")
mongoose.model("Transaction", TransactionSchema);
var Transaction = mongoose.model("Transaction");
mongoose.model("Form", FormSchema);
var Form = mongoose.model("Form");

app.post("/messages", function (req, res) {
  console.log("req.body: ", req.body);
  var form = new Form(req.body);
  form.save(function (err) {
    if (err) {
      console.log("error: ", err);
    }
    res.json(form);
  });
});
app.post("/create-transaction", function (req, res) {
  var transaction = new Transaction(req.body);
  transaction.save(function (err) {
    if (err) {
      console.log("error: ", err);
    }
    res.json(transaction);
  });
});
app.get("/transactions", function (req, res) { 
  Transaction.find({}, function (err, transactions) {
    if (err) {
      console.log("error: ", err);
    }
    res.json(transactions);
  });
});

app.post("/login",function(req,res){
  // console.log("req.body: ",req.body);
  if(req.body.email == 'admin@tedxgraphicerauniversity.com' && req.body.password == 'Tedx@ticket'){
    res.json({status:true,message:"user logged in successfully!"})
  }
  else{
    res.json({status:false,message:"Invalid credentials"})
  }
})

app.post("/verify-transaction" , function(req,res){
  Transaction.findOneAndUpdate({ transactionId: params._id },{verified:true},{returnOriginal:true}, function (err, transaction) {
    if (err) {
      console.log("error: ", err);
    }
    if(transaction){
      transaction.status=true
      res.json(transaction);
    }
    else if(!transaction){
      res.json('error')
    }
  });
})

app.post("/verify-qr",function (req, res) {
  console.log("req.body: ", req.body);
  Transaction.findOneAndUpdate({ transactionId: req.body.transactionId },{verified:true},{returnOriginal:true}, function (err, transaction) {
    if (err) {
      console.log("error: ", err);
    }
    if(transaction){
      transaction.status=true
      res.json(transaction);
    }
    else if(!transaction){
      res.json('error')
    }
  });
})

app.get("/transactions/:_id", function (req, res) { 
  console.log(req.params)
  Transaction.findOne({transactionId:req.params._id}, function (err, transactions) {
    if (err) {
      console.log("error: ", err);
    }
    res.json(transactions);
  });
});

app.get("/messages", function (req, res) {
  Form.find().sort({'created_at': -1}).exec((err,forms) => {
    if (err) {
      console.log("error: ", err);
    }
    res.json(forms);
  });
});

